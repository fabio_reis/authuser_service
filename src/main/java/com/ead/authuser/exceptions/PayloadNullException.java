package com.ead.authuser.exceptions;

public class PayloadNullException extends BusinessException {
	
	public PayloadNullException(String message) {
		super(message);		
	}

	private static final long serialVersionUID = 1L;

}
