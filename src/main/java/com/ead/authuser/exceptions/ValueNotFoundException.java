package com.ead.authuser.exceptions;

public class ValueNotFoundException extends BusinessException {

	public ValueNotFoundException(String message) {
		super(message);		
	}

	private static final long serialVersionUID = -3966218305232277785L;

}
