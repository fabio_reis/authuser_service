package com.ead.authuser.exceptions;

public class EntityRegistredException extends BusinessException {

	private static final long serialVersionUID = 1L;
	
	public EntityRegistredException(String message) {
		super(message);
	}

}
