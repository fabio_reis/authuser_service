package com.ead.authuser.api.client;

import java.util.UUID;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ead.authuser.api.dto.ResponsePageDTO;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;


//@Retry(name = "retryInstance")
@CircuitBreaker(name = "circuitbreakerInstance")
@FeignClient(name = "course", url = "${decoder.api.course.baseurl}")
public interface CourseClient {
	
	@GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE, value = "/courses")	
	public ResponsePageDTO findAllCoursesByUser(@RequestParam("userId") UUID userId);

}
