package com.ead.authuser.api.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ead.authuser.api.dto.UserRequestDTO;
import com.ead.authuser.api.dto.UserResponseDTO;
import com.ead.authuser.model.UserEntity;

@Component
public class UserMapperDTO {
	
	@Autowired
	private ModelMapper mapper;
	
	public UserResponseDTO toModelDTO(UserEntity user) {			
		return mapper.map(user, UserResponseDTO.class);	
	}
	
	public UserEntity toEntity(UserRequestDTO userRequest) {
		return mapper.map(userRequest, UserEntity.class);
	}
	
	public List<UserResponseDTO> toCollectionModelDTO(List<UserEntity> users){
		return users.stream()
				.map(user -> toModelDTO(user))
				.collect(Collectors.toList());
	}	

}
