package com.ead.authuser.api.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ead.authuser.api.config.security.JwtProvider;
import com.ead.authuser.api.dto.JwtDTO;
import com.ead.authuser.api.dto.LoginDTO;
import com.ead.authuser.api.dto.UserRequestDTO;
import com.ead.authuser.api.mapper.UserMapperDTO;
import com.ead.authuser.service.UserService;
import com.fasterxml.jackson.annotation.JsonView;

@RestController
@RequestMapping("v1/auth")
public class AuthController {
	
	Logger logger =  LogManager.getLogger(this.getClass());	
	
	final private UserService userService;	
	final private UserMapperDTO mapper;
	final private AuthenticationManager authManager;
	final private JwtProvider jwtProvider;
	
	public AuthController(
			UserService userService,
			UserMapperDTO mapper,
			AuthenticationManager authManager,
			JwtProvider jwtProvider
	) {
		this.userService = userService;
		this.mapper = mapper;
		this.authManager = authManager;
		this.jwtProvider = jwtProvider;
	}	
	
	@PostMapping("/login")
	public ResponseEntity<JwtDTO> authUser(LoginDTO loginDto) {
		
		Authentication auth = authManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword()));
		
		SecurityContextHolder.getContext().setAuthentication(auth);
		String jwt = jwtProvider.generateJwt(auth);
		return ResponseEntity.ok(new JwtDTO(jwt));
		
	}
	
	@PostMapping("signup")
	@ResponseStatus(HttpStatus.CREATED)
	public void registerUser(
			@RequestBody
			@JsonView(UserRequestDTO.UserView.RegistrationPost.class)
			UserRequestDTO userDTO) {
		userService.saveAndPublish(mapper.toEntity(userDTO));				
	}

}
