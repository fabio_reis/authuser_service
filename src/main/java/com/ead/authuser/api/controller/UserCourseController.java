package com.ead.authuser.api.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ead.authuser.api.client.CourseClient;
import com.ead.authuser.api.dto.CourseDTO;

@RestController
@RequestMapping("v1/users")
@SuppressWarnings("unchecked")
public class UserCourseController {
	
	@Autowired
	private CourseClient courseClient;		
	
	@GetMapping("{userId}/courses")
	public List<CourseDTO> findAllcoursesByUserId(
			@PathVariable() final UUID userId) {	
		
		return courseClient.findAllCoursesByUser(userId)
				.getContent();		  							
		 
	}

}
