package com.ead.authuser.api.controller;

import java.util.List;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ead.authuser.api.dto.UserRequestDTO;
import com.ead.authuser.api.dto.UserResponseDTO;
import com.ead.authuser.api.dto.UserTypeRequestDTO;
import com.ead.authuser.api.mapper.UserMapperDTO;
import com.ead.authuser.config.specificationTemplate.UserAuthSpecificationTemplate;
import com.ead.authuser.service.UserService;
import com.fasterxml.jackson.annotation.JsonView;

@RestController
@RequestMapping("v1/users")
@CrossOrigin(origins = "*", maxAge = 3600)
@SuppressWarnings("rawtypes")
public class UserController {
	
	Logger logger =  LogManager.getLogger(this.getClass());	
	
	final private UserService userService;	
	final private UserMapperDTO mapper;
	
	public UserController(UserService userService, UserMapperDTO mapper ) {
		this.userService = userService;
		this.mapper = mapper;
	}	
	
	@GetMapping	
	public ResponseEntity<?> findAll(
			UserAuthSpecificationTemplate.UserSpec spec,
			@PageableDefault(size = 10) Pageable pageable) {
		
		List<UserResponseDTO> usersDTO =
				mapper.toCollectionModelDTO(userService.findAll(spec, pageable));	
		
		return ResponseEntity.ok(new PageImpl<>(usersDTO,pageable, usersDTO.size()));		
	
	}
	
	@GetMapping("{userId}")
	public ResponseEntity findById(@PathVariable UUID userId) {
		return ResponseEntity
				.ok(mapper.toModelDTO(userService.findById(userId)));
	}	
	
	
	
	@DeleteMapping("{userId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable UUID userId) {
		userService.deleteAndPublish(userId);		
	}
	
	@PutMapping("{userId}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void updateUser(
			@PathVariable UUID userId,
			@RequestBody @JsonView(UserRequestDTO.UserView.UserPut.class) UserRequestDTO dto) {
		
		var user = userService.findById(userId);
		user.setFullName(dto.getFullName());
		user.setCpf(dto.getCpf());
		user.setPhoneNumber(dto.getPhoneNumber());
		
		userService.updateAndPublish(user);
		
	}
	
	@PutMapping("{userId}/password")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void updatePassword(
			@PathVariable UUID userId,
			@RequestBody @JsonView(UserRequestDTO.UserView.PasswordPut.class) UserRequestDTO dto) {
		
		var user = userService.findById(userId);
		user.setPassword(dto.getPassword());	
		user.setOldPassword(dto.getOldPassword());
		
		userService.updatePassword(user);
		
	}
	
	@PutMapping("{userId}/image")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void updateImage(
			@PathVariable UUID userId,
			@RequestBody @JsonView(UserRequestDTO.UserView.ImagePut.class) UserRequestDTO dto) {
		
		var user = userService.findById(userId);
		user.setImageUrl(dto.getImgUrl());			
		
		userService.updateImage(user);
		
	}
	
	@PatchMapping("{userId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void changeTypeUser(
			@PathVariable UUID userId,
			@RequestBody UserTypeRequestDTO dto) {	
		
		logger.info("Try to change userType, userId: {} to {}", userId, dto.getUserType());
		
		userService.chageUserType(userId, dto.getUserType());		
		
		logger.info("Try to change success");
		
	}

}
