package com.ead.authuser.api.config.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	private String SEC_USER = "admin"; 
	private String SEC_PASSWORD = "admin";
	
	private static final String[] URI_ACCESS_LIST = {
			"/v1/auth/**"
	};	 
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {		
		http
		.httpBasic()
		.and()
		.authorizeRequests()
		.antMatchers(URI_ACCESS_LIST).permitAll()
		.anyRequest().authenticated()
		.and()
		.csrf().disable()
		.formLogin();	
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()
		.withUser(SEC_USER)
		.password(passwordEncoder().encode(SEC_PASSWORD))
		.roles("ADMIN");
	}		
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

}
