package com.ead.authuser.api.config.security;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class JwtProvider {
	
	@Value("${auth.jwtSecret}")
	private String secret;
	
	@Value("${auth.jwtExpirationMs}")
	private int expiration;
	
	public String generateJwt(Authentication auth) {
		UserDetails userDetails = (UserDetails) auth.getPrincipal();
		
		log.info("Generating user token");
		
		return Jwts
				.builder()
				.setSubject(userDetails.getUsername())
				.setIssuedAt(new Date())
				.setExpiration(new Date(System.currentTimeMillis() + expiration))
				.signWith(SignatureAlgorithm.HS512, secret)
				.compact();
		
	}

}
