package com.ead.authuser.api.config;

import java.time.OffsetDateTime;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.ead.authuser.exceptions.EntityNotFoundException;
import com.ead.authuser.exceptions.EntityRegistredException;
import com.ead.authuser.exceptions.HttpBussinessException;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuppressWarnings({"unused","rawtypes"})
@RestControllerAdvice
public class ApiExceptionHandle extends ResponseEntityExceptionHandler {	
	
	private ErrorDetail errorDetail = new ErrorDetail();	
	
	@Getter @Setter @JsonInclude(Include.NON_NULL)
	private class ErrorDetail{		
		
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.XXX")
		private OffsetDateTime date;				
		private String detail;
		private Integer status;
		private String userMessage;				
				
	}
	
	
	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
			HttpStatus status, WebRequest request) {	
		
		errorDetail.setStatus(status.value());		
		errorDetail.setDate(OffsetDateTime.now());
		errorDetail.setUserMessage("Internal error");
		
		if(body == null) {			
			errorDetail.setDetail(status.getReasonPhrase());
			body = errorDetail;
		}else if(body instanceof String) {			
			errorDetail.setDetail((String)body);
			body = errorDetail;
		}		
		
		return super.handleExceptionInternal(ex, body, headers, status, request);
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity handleUncaught(Exception e, WebRequest request) {		
		
		errorDetail.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		errorDetail.setDetail(e.getMessage());		
		
		log.error(e.getMessage());
		
		return handleExceptionInternal(e, errorDetail, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
		
	}	
	
	
	@ExceptionHandler(EntityRegistredException.class)
	public ResponseEntity handleBadRequest(EntityRegistredException e, 
			WebRequest request) {				
		
		return handleExceptionInternal(e, e.getMessage(), new HttpHeaders(),
				HttpStatus.BAD_REQUEST, request);		
	}
	
	@ExceptionHandler({HttpBussinessException.class})
	public ResponseEntity handleHttpBussinessException(HttpBussinessException e, 
			WebRequest request) {			
		
		return handleExceptionInternal(e, e.getMessage(), new HttpHeaders(),
				HttpStatus.BAD_GATEWAY, request);		
	}
	
	@ExceptionHandler(EntityNotFoundException.class)
	public ResponseEntity handleNotFound(EntityNotFoundException e, 
			WebRequest request) {				
		
		return handleExceptionInternal(e, e.getMessage(), new HttpHeaders(),
				HttpStatus.NOT_FOUND, request);		
	}

}
