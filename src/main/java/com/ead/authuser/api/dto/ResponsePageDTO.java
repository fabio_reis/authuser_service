package com.ead.authuser.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;


@JsonIgnoreProperties(ignoreUnknown = true)
@Getter @Setter
public class ResponsePageDTO<T> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private List<T> content = new ArrayList();	
	

}
