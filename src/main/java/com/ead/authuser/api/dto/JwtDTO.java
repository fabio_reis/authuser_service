package com.ead.authuser.api.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class JwtDTO {
	
	public JwtDTO() {}
	
	public JwtDTO(String token) {
		this.token = token;
	}
	
	private String token;
	private String type = "Bearer";

}
