package com.ead.authuser.api.dto;

import java.time.OffsetDateTime;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UserResponseDTO {	
	
	private UUID userId;	
	private String userName;	
	private String email;	
	private String fullName;		
	private String phoneNumber;	
	private String cpf;	
	private String imageUrl;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
	private OffsetDateTime creationDate;		

}
