package com.ead.authuser.broker.mapper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ead.authuser.broker.dto.UserEventDTO;
import com.ead.authuser.exceptions.ParseException;
import com.ead.authuser.model.UserEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

@Component
public class UserEventMapper{	
	
	Logger logger =  LogManager.getLogger(this.getClass());
	
	@Autowired
	private ModelMapper mapper;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	public String toJson(UserEntity user) {				
		try {
			ObjectWriter ow = objectMapper.writer().withDefaultPrettyPrinter();
			return ow.writeValueAsString(mapper.map(user, UserEventDTO.class));
		} catch (JsonProcessingException e) {
			logger.error("Error try to parse: {}", user.getClass());
			throw new ParseException("Error try to parse");
		}		
	}
	
	public JsonNode toNode(UserEntity user) {				
		try {					
			ObjectWriter ow = objectMapper.writer().withDefaultPrettyPrinter();
			String json = ow.writeValueAsString(mapper.map(user, UserEventDTO.class));
			return objectMapper.readTree(json);
		} catch (JsonProcessingException e) {
			logger.error("Error try to parse: {}", user.getClass());
			throw new ParseException("Error try to parse");
		}		
	}

}
