package com.ead.authuser.broker.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.DefaultClassMapper;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ead.authuser.broker.dto.RabbitMqEventDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@Configuration
public class RabbitMqConfig {
	
	@Autowired
	private CachingConnectionFactory cachingConnectionFactory; 
	
	@Autowired
	private RabbitMqExchangeConfig exchange;
	
	@Bean
	public RabbitTemplate rabbitTemplate() {
		var template = new RabbitTemplate(cachingConnectionFactory);
		template.setMessageConverter(messageConverter());
		template.setExchange(exchange.decoderExchange);		
		return template;
	}	
	
	@Bean	
	public Jackson2JsonMessageConverter messageConverter() {		
		var objectMapper = new ObjectMapper();
		var converter = new Jackson2JsonMessageConverter(objectMapper);
		objectMapper.registerModule(new JavaTimeModule());			
		converter.setClassMapper(classMapper());
		return converter; 
	}
	
	@Bean
	public DefaultClassMapper classMapper() {
		var classMapper = new DefaultClassMapper();
		Map<String, Class<?>> idClassMapping = new HashMap<>();
		idClassMapping.put("com.ead.apiproducer.dto.RabbitMqEventDTO", RabbitMqEventDTO.class);
		classMapper.setIdClassMapping(idClassMapping);
		return classMapper;
	}
}
