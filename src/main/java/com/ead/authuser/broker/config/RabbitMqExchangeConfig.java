package com.ead.authuser.broker.config;

import org.springframework.amqp.core.FanoutExchange;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMqExchangeConfig {	
	
	@Value("${decoder.broker.exchange.fanoutDecoder}")
	public String decoderExchange;
	
	@Bean
	public FanoutExchange fanoutUserEvent() {
		return new FanoutExchange(decoderExchange);
	}

}
