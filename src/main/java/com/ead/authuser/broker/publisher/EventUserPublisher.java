package com.ead.authuser.broker.publisher;

import java.time.LocalDateTime;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ead.authuser.broker.dto.RabbitMqEventDTO;
import com.ead.authuser.broker.mapper.UserEventMapper;
import com.ead.authuser.enums.EventActionTypeEnum;
import com.ead.authuser.model.UserEntity;

@Component
public class EventUserPublisher {

	private Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	private RabbitTemplate template;

	@Autowired
	private UserEventMapper userEventMapper;

	public void sendCreateEvent(UserEntity user) {

		logger.info("Try to send userEvent save");

		var dto = new RabbitMqEventDTO(userEventMapper.toNode(user), EventActionTypeEnum.CREATE.name());

		dto.eventDate = LocalDateTime.now().toString();		
		template.convertAndSend(dto);		

		logger.info("userEvent: " + dto.action  + ": {}", dto.toString());

	}
	
	public void sendUpdateEvent(UserEntity user) {

		logger.info("Try to send userEvent update");

		var dto = new RabbitMqEventDTO(userEventMapper.toNode(user), EventActionTypeEnum.UPDATE.name());

		dto.eventDate = LocalDateTime.now().toString();		
		template.convertAndSend(dto);		

		logger.info("userEvent: " + dto.action  + ": {}", dto.toString());

	}
	
	public void sendDeleteEvent(UserEntity user) {

		logger.info("Try to send userEvent delete");

		var dto = new RabbitMqEventDTO(userEventMapper.toNode(user), EventActionTypeEnum.DELETE.name());

		dto.eventDate = LocalDateTime.now().toString();		
		template.convertAndSend(dto);		

		logger.info("userEvent: " + dto.action  + ": {}", dto.toString());

	}

}
