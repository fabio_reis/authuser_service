package com.ead.authuser.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ead.authuser.model.RoleEntity;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, UUID> {
	
	@Query(value = "SELECT * FROM tb_roles r WHERE r.role_name = :roleName LIMIT 1", nativeQuery = true)
	Optional<RoleEntity> findByRoleName(String roleName);

}
