package com.ead.authuser.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ead.authuser.model.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, UUID>, JpaSpecificationExecutor<UserEntity> {
	
	
	Optional<UserEntity> findByUserName(String userName);
	
	Optional<UserEntity> findByEmail(String email);	
	
	@Query(" FROM UserEntity u WHERE u.userId =:userId AND u.userStatus = 'ACTIVE'")
	Optional<UserEntity> findById(UUID userId);
	

}
