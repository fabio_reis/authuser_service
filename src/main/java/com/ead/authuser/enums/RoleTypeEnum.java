package com.ead.authuser.enums;

import com.ead.authuser.exceptions.ValueNotFoundException;

public enum RoleTypeEnum {
	ROLE_STUDENT("ROLE_STUDENT"),
	ROLE_INSTRUCTOR("ROLE_INSTRUCTOR"),
	ROLE_ADMIN("ROLE_ADMIN"),
	ROLE_USER("ROLE_USER");
	
	private String description;
	
	RoleTypeEnum(String description) {
		this.description = description;
	}
	
	public static RoleTypeEnum parse(String description) {
		for(RoleTypeEnum type : RoleTypeEnum.values()) {
			if(type.description.equals(description)) 
				return type;			
		}
		
		throw new ValueNotFoundException("RoleType does not exists");
	} 
}
