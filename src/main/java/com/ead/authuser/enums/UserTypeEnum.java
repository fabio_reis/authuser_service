package com.ead.authuser.enums;

import com.ead.authuser.exceptions.ValueNotFoundException;

public enum UserTypeEnum {
	
	ADMIN("ADMIN"),
	STUDENT("STUDENT"),
	INSTRUCTOR("INSTRUCTOR");
	
	private String description;
	
	UserTypeEnum(String descripion) {
		this.description = descripion;
	}
	
	public static UserTypeEnum parse(String description) {
		
		for(UserTypeEnum type : UserTypeEnum.values()) {
			if(type.description.equals(description)) 
				return type;
			
		}
		
		throw new ValueNotFoundException("UserType does not exists");
	} 
	

}
