package com.ead.authuser.enums;

public enum EventActionTypeEnum {
	
	CREATE,
	UPDATE,
	DELETE

}
