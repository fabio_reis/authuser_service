package com.ead.authuser.config.specificationTemplate;

import org.springframework.data.jpa.domain.Specification;

import com.ead.authuser.model.UserEntity;

import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.domain.Like;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;

public class UserAuthSpecificationTemplate {
	
	@And({
		@Spec(path = "userType", spec = Equal.class),
		@Spec(path = "userStatus", spec = Equal.class),
		@Spec(path = "email", spec = Like.class),
		@Spec(path = "fullName", spec = Like.class)
	})
	public interface UserSpec extends Specification<UserEntity> {}	

}
