package com.ead.authuser.config.datasource;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class AuthuserDatasourceConfigTest {			
	
	final String user = "admin";
	final String password = "admin";	
	final String host = "127.0.0.1";	
	final String port = "5432";    
	final String database = "Decoder_test";     
	final String schema = "authuser";
	
	final String url = "jdbc:postgresql://" + host +  ":" + port + "/" + database + "?currentSchema=" + schema +
			"&serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8";
	
	@Bean("dataSourceTest")	
	@Profile("test")
	public DataSource dataSource() {		
		final var hikariConfig = new HikariConfig();
		hikariConfig.setDriverClassName("org.postgresql.Driver");
        hikariConfig.setJdbcUrl(url);
        hikariConfig.setUsername(user);
        hikariConfig.setPassword(password);
        hikariConfig.setIdleTimeout(30000L);
        hikariConfig.setMaximumPoolSize(1);
        hikariConfig.setLeakDetectionThreshold(120000L);
        hikariConfig.setPoolName("authuser_pool_test");
        hikariConfig.setConnectionTestQuery("SELECT 1");        
        return new HikariDataSource(hikariConfig);		
	}
	
	@Bean("entityManagerFactoryTest")	
	@Profile("test")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
        final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        
        final Properties jpaProperties = new Properties();
        jpaProperties.setProperty("hibernate.hbm2ddl.auto", "none");
        jpaProperties.setProperty("hibernate.jdbc.lob.non_contextual_creation", "true");
        jpaProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");       
        jpaProperties.setProperty("show_sql", "true");
        jpaProperties.setProperty("format_sql", "true");
        jpaProperties.setProperty("use_sql_comments", "true");
        jpaProperties.setProperty("open-in-view", "false");              
        
        final LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setPackagesToScan("com.ead.authuser");
        factory.setDataSource(dataSource);
        factory.setJpaProperties(jpaProperties);
        return factory;
    }
	
	@Bean("transactionManagerTest")	
	@Profile("test")
	public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
	    final var txManager = new JpaTransactionManager();
	    txManager.setEntityManagerFactory(entityManagerFactory);
	    return txManager;
	}	

}
