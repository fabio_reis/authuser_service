package com.ead.authuser.utils;

import org.jdbi.v3.core.statement.StatementContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.lang.String.format;

public class SqlLoggerImpl implements org.jdbi.v3.core.statement.SqlLogger {
	private final Logger logger = LoggerFactory.getLogger(SqlLoggerImpl.class);
	
	@Override
	public void logAfterExecution(StatementContext context) {
		if (logger.isInfoEnabled()) {
			logger.info(format("Query after execution: %s", context.getParsedSql().getSql()));
		}
	}
}
