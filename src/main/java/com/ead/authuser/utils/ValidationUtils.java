package com.ead.authuser.utils;

import org.springframework.util.ObjectUtils;

public class ValidationUtils {
	
	public static boolean isNull(Object value) {
		if(ObjectUtils.isEmpty(value)) return true;
		return false;
	}	
	
	public static Object returnValueOrNull(Object value) {
		if(ObjectUtils.isEmpty(value)) return null;
		return value;
	}

}
