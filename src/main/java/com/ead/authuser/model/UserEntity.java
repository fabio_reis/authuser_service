package com.ead.authuser.model;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.ead.authuser.enums.UserStatusEnum;
import com.ead.authuser.enums.UserTypeEnum;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="TB_USERS")
@Setter @Getter @EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class UserEntity {	
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id")
	@EqualsAndHashCode.Include
	private UUID userId;
	
	@Column(name="user_name", nullable = false, unique = true, length = 50)
	private String userName;
	
	@Column(nullable = false, unique = true, length = 50)
	private String email;
	
	@Column(nullable = false, length = 255)
	private String password;		
	
	@Column(name = "old_password")
	private String oldPassword;
	
	@Column(name="full_name", nullable = false, length = 150)
	private String fullName;
	
	@Enumerated(EnumType.STRING)
	@Column(name="user_status", nullable = false)
	private UserStatusEnum userStatus;
	
	@Enumerated(EnumType.STRING)
	@Column(name="user_type", nullable = false)
	private UserTypeEnum userType;
	
	@Column(name="phone_number", length = 20)
	private String phoneNumber;
	
	@Column(length = 20)
	private String cpf;
	
	@Column(name="image_url")
	private String imageUrl;
	
	@CreationTimestamp
	@Column(name="creation_date", nullable = false)
	private OffsetDateTime creationDate;
	
	@UpdateTimestamp
	@Column(name="last_update_date", nullable = false)
	private OffsetDateTime lastUpdateDate;		
	
	@ManyToMany
	@JoinTable(
		name = "TB_USERS_ROLES",
		joinColumns = @JoinColumn(name="user_id"),
		inverseJoinColumns = @JoinColumn(name="role_id")		
	)
	private List<RoleEntity> roles = new ArrayList<>();

}
