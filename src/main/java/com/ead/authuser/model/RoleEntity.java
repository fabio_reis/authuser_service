package com.ead.authuser.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

import com.ead.authuser.enums.RoleTypeEnum;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "TB_ROLES")
@Getter @Setter @EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class RoleEntity implements GrantedAuthority {	
	
	private static final long serialVersionUID = 1L;

	@Id @Column(name = "role_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@EqualsAndHashCode.Include	
	private UUID roleId;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "role_name", nullable = false)
	private RoleTypeEnum roleName;

	@Override
	public String getAuthority() {		
		return this.roleName.toString();
	}
	
	

}
