package com.ead.authuser.service;

import static com.ead.authuser.utils.ValidationUtils.isNull;

import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.ead.authuser.broker.publisher.EventUserPublisher;
import com.ead.authuser.enums.RoleTypeEnum;
import com.ead.authuser.enums.UserStatusEnum;
import com.ead.authuser.enums.UserTypeEnum;
import com.ead.authuser.exceptions.EntityNotFoundException;
import com.ead.authuser.exceptions.EntityRegistredException;
import com.ead.authuser.model.RoleEntity;
import com.ead.authuser.model.UserEntity;
import com.ead.authuser.repository.UserRepository;

@Service
public class UserService {	
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private EventUserPublisher publisher;
	
	public List<UserEntity> findAll(Specification<UserEntity> spec, Pageable pageable){
		Page<UserEntity> pageUser = userRepository.findAll(spec, pageable);
		return pageUser.getContent();
	}	
	
	public UserEntity findById(UUID userId) {
		return userRepository.findById(userId)
				.orElseThrow(
						() -> new EntityNotFoundException(String.format("User not found: %s", userId)));
	}
	
	@Transactional
	public UserEntity delete(UUID userId) {
		var user = findById(userId);
		userRepository.delete(user);
		return user;
	}	
	
	@Transactional
	public UserEntity save(UserEntity user) {
		
		if(existsByUserName(user.getUserName()))
			throw new EntityRegistredException(String.format("Username already registred", user.getUserName()));
		
		if(existsByEmail(user.getEmail()))
			throw new EntityRegistredException(String.format("E-mail already registred", user.getEmail()));
		
		RoleEntity role =
				roleService.findByRoleName(RoleTypeEnum.ROLE_STUDENT.toString());	
		
		userValidation(user);	
		
		user.setUserStatus(UserStatusEnum.ACTIVE);
		user.setUserType(UserTypeEnum.STUDENT);		
		user.setPassword(passwordEncoder.encode(user.getPassword()));	
		user.getRoles().add(role);
		return userRepository.save(user);		
	}	
	
	@Transactional
	public UserEntity update(UserEntity user) {		
		userValidation(user);			
		return userRepository.save(user);				
	}
	
	public void updateImage(UserEntity user) {
		
		if(isNull(user.getImageUrl()))
			throw new EntityRegistredException("imageUrl empty");
		
		update(user);
	}
	
	@Transactional	
	public void deleteAndPublish(UUID userId) {
		var user = delete(userId);
		publisher.sendDeleteEvent(user);					
	}
	
	@Transactional
	public void saveAndPublish(UserEntity user) {
		var userCreated = save(user);
		publisher.sendCreateEvent(userCreated);			
	}
	
	@Transactional
	public void updateAndPublish(UserEntity user) {
		var userUpdated = update(user);
		publisher.sendUpdateEvent(userUpdated);			
	}	
	
	public void updatePassword(UserEntity user) {
		
		if(isNull(user.getPassword()))
			throw new EntityRegistredException("password empty");
		
		if(isNull(user.getOldPassword()))
			throw new EntityRegistredException("oldPassword empty");
			
		var userDatabase = findById(user.getUserId());							
		
		if(!user.getOldPassword().equals(userDatabase.getOldPassword()))
			throw new EntityRegistredException(String.format("Error oldPassword", user.getOldPassword()));
		
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		
		update(user);
		
	}
	
	@Transactional
	public void chageUserType(UUID userId, String userType) {
		
		if(isNull(userType)) 
			throw new EntityRegistredException("userType empty");
		
		UserEntity userEntity = findById(userId);
		
		userEntity
			.setUserType(UserTypeEnum.parse(userType));
		
		userRepository.save(userEntity);
	}
	
	private boolean existsByUserName(String userName) {				
		var user =  userRepository.findByUserName(userName)
		.orElse(null);		
		return user == null ? false : true;		
	}
	
	private boolean existsByEmail(String email) {		
		var user =  userRepository.findByEmail(email)
		.orElse(null);		
		return user == null ? false : true;		
	}
	
	private void userValidation(UserEntity user) {			
			
		if(isNull(user.getPassword()))
			throw new EntityRegistredException("password empty");
		
		if(isNull(user.getUserName()))
			throw new EntityRegistredException("userName empty");
		
		if(isNull(user.getEmail()))
			throw new EntityRegistredException("email empty");
		
		if(isNull(user.getFullName()))
			throw new EntityRegistredException("fullName empty");	
		
	}

}
