package com.ead.authuser.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ead.authuser.exceptions.EntityNotFoundException;
import com.ead.authuser.model.RoleEntity;
import com.ead.authuser.repository.RoleRepository;

@Service
public class RoleService {
	
	@Autowired
	private RoleRepository roleRepository;
	
	public List<RoleEntity> findAll(){
		return roleRepository.findAll();
	}
	
	public RoleEntity findByRoleName(String roleName){
		return roleRepository.findByRoleName(roleName)
				.orElseThrow(() -> new EntityNotFoundException("Role not found"));
	}

}
