create table TB_ROLES (
    role_id uuid not null,
    role_name VARCHAR(40) UNIQUE NOT NULL,
    primary key (role_id)
)