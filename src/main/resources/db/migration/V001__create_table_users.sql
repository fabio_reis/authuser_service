create table tb_users (
    user_id uuid not null,
    cpf varchar(20),
    creation_date timestamp not null,
    email varchar(50) not null unique,
    full_name varchar(150) not null,
    image_url varchar(30),
    last_update_date timestamp not null,
    password varchar(30) not null,
    old_password varchar(30),
    phone_number varchar(20),
    user_name varchar(50) not null unique,
    user_status varchar(30) default 'ACTIVE' not null,
    user_type varchar(30) default 'STUDENT' not null,
    primary key (user_id)
)