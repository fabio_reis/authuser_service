TRUNCATE TABLE auth_user.TB_USERS CASCADE;
TRUNCATE TABLE auth_user.TB_ROLES CASCADE;
TRUNCATE TABLE auth_user.TB_USERS_ROLES CASCADE;

INSERT INTO auth_user.tb_users(
	user_id,
	cpf,
	creation_date,
	email,
	full_name,
	image_url,
	last_update_date,	
	phone_number,
	user_name,
	user_type,
	old_Password,
	password	
)
VALUES(
	(SELECT md5(random()::text || clock_timestamp()::text)::uuid),
	'05561373536',
	timezone('utc', now()),
	'fabio_dos_reis@outlook.com',
	'Fábio Reis',
	'image',
	timezone('utc', now()),	
	'11970419997',
	'fabiorv',
	'ADMIN',
	'123',
	'123'	
);

INSERT INTO auth_user.tb_users(
	user_id,
	cpf,
	creation_date,
	email,
	full_name,
	image_url,
	last_update_date,	
	phone_number,
	user_name,
	user_type,
	password
)
VALUES(
	(SELECT md5(random()::text || clock_timestamp()::text)::uuid),
	'13905589089',
	timezone('utc', now()),
	'carol_cunha@outlook.com',
	'Caroline Cunha',
	'image',
	timezone('utc', now()),	
	'77987835568',
	'mcarolc',
	'ADMIN',
	'123'
);

INSERT INTO auth_user.tb_roles(
	role_id,
	role_name	
)
VALUES(
	(SELECT md5(random()::text || clock_timestamp()::text)::uuid),
	'ROLE_STUDENT'
);

INSERT INTO auth_user.tb_roles(
	role_id,
	role_name	
)
VALUES(
	(SELECT md5(random()::text || clock_timestamp()::text)::uuid),
	'ROLE_INSTRUCTOR'
);

INSERT INTO auth_user.tb_roles(
	role_id,
	role_name	
)
VALUES(
	(SELECT md5(random()::text || clock_timestamp()::text)::uuid),
	'ROLE_ADMIN'
);

INSERT INTO auth_user.tb_roles(
	role_id,
	role_name	
)
VALUES(
	(SELECT md5(random()::text || clock_timestamp()::text)::uuid),
	'ROLE_USER'
);

INSERT INTO auth_user.tb_users_roles(
	user_id,
	role_id
)
VALUES(
	(SELECT u.user_id FROM auth_user.tb_users u ORDER BY user_id ASC LIMIT 1),
	(SELECT r.role_id FROM auth_user.tb_roles r ORDER BY role_id ASC LIMIT 1)
)
