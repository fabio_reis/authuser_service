package com.ead.authuser.api.controller;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@SpringBootTest(	
	webEnvironment = WebEnvironment.RANDOM_PORT	 
) 
public class UserControllerTest {	
	
	@Test
	@DisplayName("test")
	public void test() {
		
		assertThat(true);
	}

}
