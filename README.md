# AUTHUSER_SERVICE

The ***Kestraa Documents*** project is responsible for creating, editing and deleting.

```sh
- Test
```

<div align="center">

```sh
Platform: JVM (Java)
Java Version: 11
Build System: Maven
```


</div>

## Git
* git clone http etc


## How to run local
* Local URL: http://localhost:{PORT}

### Local
* HTTP_PORT: 8080
* Ensure that the local profile is active in application.yml
* Run the application in your IDE


## Environment
**Default**: Dev Environment Values
### 
* SPRING_PROFILES_ACTIVE: local
* KESTRAA_SHIPMENT_SERVICE_BASEURL: test


### RabbitMq
* RABBIT_HOST: test
* RABBIT_PORT: 5672
* RABBIT_USR: default
* RABBIT_PWD: default

### Database
* DATABASE_HOST:test
* DATABASE_PORT: 5432


#### How to apply this environment variables

To understand how to apply the development variables without having to create a file just for that, click on this documentation: [Import environment variables]

## Build
Some interesting Maven *targets* (build system):

* `./mvm install`: Dependencies download
* `./mvm --build`: Full project build (with dependencies download too)
* `./mvm clean`: Clean build full project

<div>
    <h2>Tips</h2>
</div>


## Architecture

This project was built using **JAVA**

### Used libraries:

* **Maven**: Build tool and dependency manager.
* **Springframework-boot**: Web framework. (API)
* **springframework-cloud**: Feign
* **Springframework-data**: Provides convenient, idiomatic access to relational data in Java.
* **JPA**: ORM data persistence(Hibernate).

